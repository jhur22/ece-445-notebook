# ECE 445 Notebook

## Joshua Worklog
- [January 29th, 2024: Discussed Project with Machine Shop](###-January-29th,-2024:-Discussed-Project-with-Machine-Shop)
- [February 5th, 2024: Discussed Project Details with Machine Shop](###-February-5th,-2024:-Discussed-Project-Details-with-Machine-Shop)
- [February 6th, 2024: Received Original Magnet from Equipment](###-February-6th,-2024:-Received-Original-Magnet-from-Equipment)
- [February 7th, 2024: Worked on Project Proposal](###-February-7th,-2024:-Worked-on-Project-Proposal)
- [February 8th, 2024: Discussion about Power Subsystem](###-February-8th,-2024:-Discussion-about-Power-Subsystem)
- [February 13th, 2024: Discussed with TA about Power Subsystem Issue](###-February-13th,-2024:-Discussed-with-TA-about-Power-Subsystem-Issue)
- [February 19th, 2024: Worked on Design Document](###-February-19th,-2024:-Worked-on-Design-Document)
- [February 21st, 2024: Brainstormed Idea for Magnetic Switch](###-February-21st,-2024:-Brainstormed-Idea-for-Magnetic-Switch)
- [March 3rd, 2024: Found N-Channel MOSFET for Magnet Switch](###-March-3rd,-2024:-Found-N-Channel-MOSFET-for-Magnet-Switch)
- [March 8th, 2024: Tried Initializing Raspberry PI](###-March-8th,-2024:-Tried-Initializing-Raspberry-PI)
- [March 20th ~ 21st, 2024: Grabbed Additional Components for Project](###-March-20th-~-21st,-2024:-Grabbed-Additional-Components-for-Project)
- [March 23rd, 2024: Soldering PCB v1.0](###-March-23rd,-2024:-Soldering-PCB-v1.0)
- [March 24th, 2024: Explosion of PCB v1.0](###-March-24th,-2024:-Explosion-of-PCB-v1.0)
- [April 2nd, 2024: Ordering PCB parts for v2.0 & 3.0](###April-2nd,-2024:-Ordering-PCB-parts-for-v2.0-&-3.0)
- [April 3rd, 2024: Buck Converter Voltage/Wavelength Measurements & Stepper Motor Comparisons](###-April-3rd,-2024:-Buck-Converter-Voltage/Wavelength-Measurements-&-Stepper-Motor-Comparisons)
- [April 12th, 2024: Update Magnetic Switch Design](###-April-12th,-2024:-Update-Magnetic-Switch-Design)
- [April 20th, 2024: Finished Chess AI Functions](###-April-20th,-2024:-Finished-Chess-AI-Functions)
- [April 22 ~ 23rd, 2024: Finishing Touches on A*](###-April-22-~-23rd,-2024:-Finishing-Touches-on-A*)
- [Final Project: How it Looks in the End](###-Final-Project:-How-it-Looks-in-the-End)

### January 29th, 2024: Discussed Project with Machine Shop
- Discussed with Gregg Bennett, Machine Shop Supervisor, about the AI Chess Robot's hardware specs. There was a pre-existing piece of equipment that satisfied what we were looking for stored in the basement of ECEB. On the equipment, there was a rail/magnet system consisting of:
    - (3) Mercury Motor SM-42BYG011-25 2 Phase 1.8° 32/20
    - (1) KK P-50/27 50 kg Electromagnet
- Lets say we view the system from the top with x and y-axis. Two of the stepper motors are responsible for x-direction rails with the third motor responsible for the y-direction and resting on both of the other two perpendicularly to prevent drift when the x-axis motors move. The magnet rests on the third motor underneath the board to grab metal pieces. Unfortunately, the magnet tends to be weak or unreliable. Will ask for elaborate explanation on a later date.
![Chess board the machine shop had in the basement](images/chess_board_before.jpg)

### February 5th, 2024: Discussed Project Details with Machine Shop
- Discussed further about the equipment and its issues that pertain to it. Received a smaller version of the magnet connected to the equipment, and one 1/4" and one 1/8" plexiglass to experiment with. Asked to add modifications to the equipment. We needed an arm or something to hold a small MIPI camera above and looking down towards the center of the board. We need to brainstorm board size, thickness, and how we want it designed.
- Magnet Inconsistencies:
    - Ex: Magnet turns on in the center of a metal piece. It will grab it, but when traveling left, the piece will get dragged on the right side of the magnet. Seems to have a distance offset opposite to the movement vector.
- Board Issues:
    - Board needs to have a certain thickness/rigidness to prevent the magnet from grabbing a piece, which can cause the board to get "grabbed" and bent towards the magnet. Once the magnet deactivates, the board may "snap" back to its original shape causing pieces on top to get displaced.

### February 6th, 2024: Received Original Magnet from Equipment
- Went back to the machine shop to remove and grab the original magnet from the equipment to experiment with. Cannot experiment this week because the lab was closed due to its cleanliness. 

### February 7th, 2024: Worked on Project Proposal
- Worked on the ethics and safety of the project proposal. Referred to GPL v3 license limitations and restrictions because the python-chess library has it. To ensure we were in line with proper engineering ethics, I referred to the Committee on Professional Ethics' (COPE) code of ethics. Additionally, because we are using MIPI technology and Raspberry Pi, we need to follow their rules and regulations, such as logo usage and commercialization.

### February 8th, 2024: Discussion about Power Subsystem
- Regarding the power system, we need a three different voltage ratings:
    - 12V
    - 5V
    - 3.3V
- We concluded that there could be two different options as of right now. We will discuss with a more knowledgeable TA to help us.
    1. Series/Parallel Resistors: Use a series and parallel of resistors to lower the voltage to desired levels. 
        - Pros: Cheap resistors
        - Cons: Not efficient (loss in heat, etc), might crowd the pcb 
    2. AC to DC Adapter: Use the adapter to directly change voltage.
        - Pros: Easy to adapt to the board, pre-made parts decrease soldering workload
        - Cons: Expensive with budget conscientious-plan 

### February 13th, 2024: Discussed with TA about Power Subsystem Issue
- Discussed with TA about our power subsystem issue with the three different voltages we needed for the project. He proposed a step-down converter. It utilizes a diode and capacitors to smooth out voltage changes (the average of the fluctuations). We need to look at converters for 5V and 3.3V and check their datasheets for the recommended resistor, capacitor, and etc values needed. We will likely need two of these and have their own dedicated lines on the pcb design to send their respective voltages where it is needed.

### February 19th, 2024: Worked on Design Document
- Worked on the requirements and verifications list for the Visual and Magnetic Arm subsystem, and customized the entire syntax design of the subsystem requirements and verifications list. Next immediate plan is to visit the machine shop/E-shop for free washers/metal pieces for our chess pieces.

Ask machine shop hours of labor, cost of washers/how many they have (for free, if at all), material usage
Ask E-shop for free washers, buttons (ask for static 1 hazard)

### February 21st, 2024: Brainstormed Idea for Magnetic Switch
- Unsure of how to inform the magnet to switch from on to off and visa versa based on if the chess AI finished its move or wants to reset its position, there were 2 ideas I thought of.
    1. Diode AND Logic Gate:
        - The idea is to use the GPIO from the ESP32 S3 microcontroller and the 12V power bus as inputs to the AND gate. The idea was to always have the 12V keeping one of the diodes as "on" or high and the GPIO from the microcontroller as our controllable input variable. Whenever we would want the magnet to be powered to the 12V source, we would send a 3.3V through the GPIO, and as a result, it would satisfy the AND gate logic and send through volts. 
        - Considered how there is voltage drop across diodes so the output voltage might be lower than the input of 12V. The diodes themselves need to be considered, specifically their voltage ratings as one would have to deal with 12V and the other would have to deal with 3.3V and their tolerances. Other concerns are temperature characteristics (i.e. when the diode gets to x degrees, how does it function?) and load considerations.
    2. MOSFET:
        - The idea is to choose a MOSFET, currently thinking a N-channel MOSFET:
            - Drain (D): 12V Supply
            - Source (S): Magnet 
            - Gate (G): 3.3V GPIO
        - When the gate receives 3.3V, the MOSFET should be set to "conduct" and allow the 12V source to flow through to the magnet. This would allow it to act as a "switch" and allow it to be controllable with the microcontroller.
        - More on N-channel v. P-channel:
            -   |N-channel|P-channel|
                |:---:|:---:|
                |Typically cheaper than P-channels with same resistance|Typically more expensive than N-channels with same resistance|
                |Widely used so more availability|Not as common as N-channel|
                |Typically require positive gate-source voltage to turn on|Typically require negative gate-source voltage to turn on|

### March 3rd, 2024: Found N-Channel MOSFET for Magnet Switch
- Searched for N-channel MOSFETS that would fit our power criteria. Big reason why it was chosen over P-channel is because N-channels are used more therefore are cheaper than their counterparts.
- List of N-channel MOSFETs:
    - No MOSFET driver needed, but current is high:
        - https://www.mouser.com/datasheet/2/196/Infineon_IRLML6344_DataSheet_v01_01_EN-3363406.pdf
    - MOSFET driver needed, but current is fine:
        - https://www.vishay.com/docs/91127/sihfd110.pdf
        - https://www.diodes.com/assets/Datasheets/DMN13H750S.pdf
    - No MOSFET driver needed, current fine:
        - https://www.mouser.com/datasheet/2/916/BSH103BK-2065541.pdf
- Decided on a MOSFET that needs no driver and that has acceptable current.
    - V_GS(th) is between 0.75V - 1.25V which the 3.3V can fully saturate/turn on.
    - I_D is between 0.6A - 1A depending on temperature which is an acceptable range of amps.
- Found MOSFET has the following characteristics:
    - ![V_th (on)](images/MOSFET_gate_source_voltage_threshold.png)
    - ![I_D (Drain Current)](images/MOSFET_drain_current.png)

### March 8th, 2024: Tried Initializing Raspberry PI
- Received the Raspberry PI 4 Model B and needed to verify if it could be registered/booted without micro HDMI cable and micro SD.
- From [setting up Raspberry PI without HDMI and ethernnet cable](https://forums.raspberrypi.com/viewtopic.php?t=281674), it mentioned:
    - 1. Buying a micro HDMI cable.
    - 2. Setting up the Raspberry PI before first boot by sending a copy of "wpa_supplicant.conf" and empty "ssh" file to the /boot folder.
        - It seems a Raspberry PI 4 can be programmed with the file "wpa_supplicant.conf" and empty "ssh" file in /boot directory. However, it seems a micro HDMI cable is needed to visually interact with the Raspberry PI's OS.
    - 1. Set up on PI 3 with Buster download, networking, and download as usual
    - 2. Run code:
        - '''
        sudo apt update
        sudo apt full-upgrade '''
    - 3. Shutdown the PI, transfer the card, and boot PI 4.
    - 4. Because of no video access, need to access PI4B via VNC. 
        - It seems that using another PI do program the PI 4 is possible. Issue is that we have no other PI to program the PI4B with.
- From [youtube video about programming PI over internet](https://www.youtube.com/watch?v=shChBhapdTo), it mentioned:
    - 1. Using Raspberry PI's bootloader/imager to connect and program the PI directly via ethernet cable without SD card.
        - Issue is that he used a micro HDMI to view the Raspberry PI 4's interface.
- Tried myself to program/boot the Raspberry PI with my own ethernet cable and no micro HDMI cable and micro SD. Unfortunately, I tried reading the IP address of the Raspberry PI on both my internet provider and computer via commands such as: '''ipconfig /all''' or '''ifconfig'''. Unfortunately, I could not find the IP address of the PI in both places, so I decided that it is either difficult or next to impossible to boot/program or SSH into the PI without either the micro SD or micro HDMI.
- Informed team members that both the micro HDMI and SD is recommended to go further in development of the Raspberry PI.

### March 20 ~ 21st, 2024: Grabbed Additional Components for Project
- Went to Electronics Service Shop and Supply Center to find parts.
- Not Free:
    - ======================== Capacitors =======================
        - (3) 100 uF capacitor (bulk/tantalum) (motor drivers)
        - (4) 0.47 uF capacitor (ceramic) ((1) linear reg, (3) motor drivers)
        - (3) 0.22 uF capacitor (ceramic) (motor drivers)
        - (2) 47 uF capacitor (ceramic) (buck converter)
    - ======================== Misc =============================
        - (1) Schottky diode (magnet switch)
- Free:
    - ======================== Capacitors =======================
        - (1) 33 uF capacitor (ceramic) (linear regulator) -> (1) 47 uF capacitor (tantalum) (linear regulator)
        - (3) 1 uF capacitor (ceramic) ((2) esp32, (1) debouncing circuit for button)
        - (3) 10 uF capacitor (bypass/tantalum) ((2) buck converter, (1) esp32)
        - (3) 0.1 uF capacitors (ceramic) (motor drivers)
        - (3) 0.022 uF capacitors (ceramic) (motor drivers)
        - (6) 0.01 uF capacitors (ceramic) (motor drivers)
        - (3) 100 nF capacitor (ceramic) (buck converter)
    - ======================== Resistors ========================
        - (3) 100 kΩ resistor ((2) buck converter, (1) esp32)
        - (4) 1 kΩ resistor ((2) esp32, (2) I/O for LEDs)
        - (5) 10 kΩ resistor ((1) buck converter, (3) motor driver, (1) debouncing circuit)
- Received Raspberry PI 4, but there is no micro SD nor micro HDMI cord. Tried SSH into the Raspberry PI with ethernet and power, and it seems that it is impossible because a micro SD is needed to allow the SSH. Micro HDMI is optional if SSH is possible but will see if it is needed in the future. Asked for micro SD.

### March 23rd, 2024: Soldering PCB v1.0 
- Soldered PCB v1.0 with through hole components:
    - 3 stepper motor drivers
    - ESP32-S3 WROOM
    - Buck converter
    - Linear regulator
    - 2.1 mm barrel Jack 
    - Capacitors, such as the 0.01uF ceramic capacitors from the ECE Supply Shop (more seen in "/images/parts") ![0.01uF ceramic capacitors from the ECE Supply Shop](/images/parts/capacitor_0.01uF_ceramic.jpg)
    - Inductors
    - Resistors
- Specifically for the buck converter and drivers, the pins were too small for manual hand soldering so it was baked in the PCB oven.
    - Because the oven had to be programmed to specific temperatures and time durations, the paste's details were located [in this datasheet](https://www.chipquik.com/datasheets/TS391AX50.pdf).
    - Used the microscope available in the oven room to view the correct orientation of the buck converter and drivers, which was checking for the dot.
    
### March 24th, 2024: Explosion of PCB v1.0
- Turns out that SOME capacitors are polarized. When the 12 V barrel jack made contact with the power adapter from the wall outlet, funny aroma filled the air shortly followed by an explosion of the capacitors by the buck converter. Miraculously the capacitors everywhere else, such as the drivers and ESP32, did not blow up. The drivers were polarized so luckily all three were fine while the ESP32 had a mix of both.
- Planned to replace components that were ruined and revise PCB.

### April 2nd, 2024: Ordering PCB parts for v2.0 & 3.0
- After finding out that a LOT of surface mount components from the ECE Supply Shop could be ordered FOR FREE through the TA and walking down to the shop, decided to order surface mount replacements of all our surface mounts to prepare for PCB v2.0 and PCB v3.0. 
- Parts list for TA consisted:
    - Resistors (Ω):
        - (1) 23.7 k
        - (1) 27.4 k
        - (4) 1 k
        - (1) 1.5 k
        - (9) 10 k
        - (3) 100 k
    - Capacitors (uF):
        - (6) 0.01
        - (6) 0.1
        - (3) 1
        - (3) 10
        - (3) 33
- Everything else was checked from the in-person visit to the ECE Supply Shop.

### April 3rd, 2024: Buck Converter Voltage/Wavelength Measurements & Stepper Motor Comparisons
- After PCB v1.0 was salvaged and fixed after the explosion incident, checked the buck's switching voltage and frequency with the oscilloscopes/multimeters present in the ECE 445 lab. The buck unfortunately provided an output of 2.6 V with the load resistor providing 36.8 kΩ rather than the calculated 52.5 kΩ. Although the resistor behavior appears to be normal according to online searches, the buck appears to be having unintended behaviors. 
- Realized that the buck converter needs a 33 nF capacitor to act properly, and once it was placed on the PCB, it outputted a more acceptable range of 5.675 V but should shrink back to around 5 V due to resistors and loads.
- SW pin of the buck outputted 0 Hz which indicates a broken buck or improper connections.
- FB pins measures at 0.9 V rather than being equal to less than 0.8 V.   
- Will review datasheet for buck and seek help from TAs.
- According to the datasheet for the Mercury Motor SM-42BYG011-25 2 Phase 1.8° 32/20, its time constant τ is inductance (H)/resistance (Ω).
    - From ![this image](/images/stepper_motor_math1.png), the coil will charge to 63% of its rated value in 0.984 ms to 1.804 ms.
- For comparisons, I viewed Sanyo Denki SS2422-5041 from [here](https://uk.farnell.com/sanyo-denki-sanmotion/ss2422-5041/42mm-slim-pancake-stepper-motor/dp/1708573) to observe the difference of capabilities.
    - From ![this image](/images/stepper_motor_math2.png), comparing the time constant of our stepper versus the compared stepper, we see that the compared stepper has a time constant that is 57.83 ± 12.41% faster at charging its coil than ours. This difference does not matter for lower speed torques, [as time constant does not matter as much for these values](https://www.orientalmotor.com/stepper-motors/technology/stepper-motor-basics.html#:~:text=Inductance%20affects%20high%20speed%20torque). However, for higher speed torques, this does matter. Unfortunately, these are the stepper motors provided to us by the machine shop, and we do not have the budget for new ones, so our chess robot may move chess pieces slower than what is ideal. 

### April 12th, 2024: Update Magnetic Switch Design
- Realized from external advice that the MOSFET's design was incorrect from the beginning. Instead of ![this version of the MOSFET switch](/images/wrong_magnet_switch.png) it should have instead been ![this version of the MOSFET switch](/images/right_magnet_switch.png).
- The issue with the incorrect MOSFET is that the magnet was connected to the high-side of the load rather than the low side. 
- With the correct design, when 3.3 V is applied to the gate, we saw 2.2 V because the threshhold voltage was approximately 1.1 V. Reason is if any decent amount of current flowed from source to load, the gate-source voltage would decrease and turn off the MOSFET. Connecting it to the low-side allows 3.3 V to the gate to run to source regardless of load. 

### April 20th, 2024: Finished Chess AI Functions
- Although the Chess AI was worked on frequently before now, it was finished at this point in time and should be explained about its capabilities and functions.
- All functions of the Chess AI:
    - FEN_check(FEN_string):
        - Checked whether the FEN_string inputted into the function was valid or invalid.
        - Because future functions base their logic on this FEN_string, it needs to be able to handle errors so a try and except was implemented and can be seen here ![here](/images/FEN_string_check.png)
    - cheat_check(FEN_string):
        - Checked whether the user had cheated during their ply.
        - Processes a FEN_string from a previous game state and applies all possible plys from that state. It then applies all plys one at a time and checks if it matches the FEN_string from Computer Vision.
            - If it matches then it is possible to reach the current game state from the previous one. No cheating.
            - If there are no matches, it is not possible to reach the current game state from the previous one. Cheating/discrepancies.
        - A portion of the code can be seen here![here](/images/Cheat_check.png)
    - eval_best_move(FEN_string, which_algorithm)
        - Picks an algorithm to evaluate and produce chess moves depending on the which_algorithm parameter.
        - FEN_string is needed for the algorithm to visualize the state of the game.
        - Code can be seen here ![here](/images/Eval_move.png)
    - Stockfish_eval(FEN_string):
        - Evaluates a best move from the open Chess engine Stockfish.
            - Uses backtracking game theory algorithm Minimax.
            - Streamlines time complexity with Alpha-Beta Pruning where it compares the Alpha and Beta parameters at each node, and if it fails the check, it prunes/cuts the rest of the branch and proceeds to other nodes.
            - Contains a depth parameter that dictates how thorough it is in the evaluation.
                - Watch this parameter as Minimax is exponentially expensive based on the depth of the tree searched.
        - Uses FEN_string to visualize the current game state.
        - Code can be seen here ![here](/images/Stockfish_eval.png)
    - special_move_check(FEN_string, best_move):
        - Received the move, it checks if the move contains any special properties that need to be considered.
            - Special properties include:
                - Not a special move
                - En Passant
                - Castling
                - Capture
                - Promotion
        - FEN_string is used to check the special cases.
        - Portion of code can be seen here ![here](/images/Special_move_check.png)
    - prep_info_to_ESP(FEN_string, best_move_processed):
        - Received the move and its special properties flag, it preps the info for the ESP32. 
        - For example, the source and destination locations for chess pieces are separated by spaces (i.e. e2e5 -> e 2 e 5).
        - FEN_string is sent to the ESP32 to help visualize the matrix of "walls" and open spaces.
        - Portion of code can be seen here ![here](/images/Process_info.png)
    - chess_AI(FEN_string, which_alg):
        - Calls all of the above functions
        - Code can be seen here ![here](/images/chess_AI_func.png)
- All of these will be utilized by Computer Vision and then processed and sent to the ESP32 for motor and electromagnet use.

### April 22 ~ 23rd, 2024: Finishing Touches on A*
- During this last push, Computer Vision worked on their portion while ESP32 worked on theirs. ESP32 motor control had issues where it would choose paths that were populated with many chess pieces which significantly increased the chances of the electromagnet gripping other unintended chess pieces. As a result, the A*'s heuristic logic had to be adjusted to avoid those routes while choosing the shortest path. Rather than looking at its immediate neighbors and choosing a route from there, it views all neighbors AND their neighbors and checks the amount of pieces or "walls" are there. This way it will choose paths that have less pieces further down the path.
- An example of paths taken is here ![here](/images/updated_A_star.png). In this image, the piece needs to be moved to the top-left corner of the board to be removed. The orange path is the updated A* avoiding clusters of chess pieces as it moves while the blue path is the original A* where it could grab the cluster of chess pieces along the path.

### April 25th, 2024: Worked on Final Presentation and Mock Presentation
- Worked on the Chess AI portion of the final presentation before the mock presentation. Added details regarding how it parses through the FEN_strings from Computer Vision, processes it, and sends it to the ESP32.
- Feedback from the TAs tells me that:
    - Content-wise, more information regarding the PCB (hardware) should be expressed.
    - Presentation-wise, add transitions between speakers, increase font size for audience members with vision troubles, and do not block other speakers while they speak. 

### April 26th, 2024: Worked on Extra Credit Video
- Recorded an audio file where I explained the Chess AI very briefly. Explained the background of Stockfish as well.

### Final Project: How it Looks in the End
- Here is how the project looks in the end ![project all together at the very end](/images/End_project.png).



